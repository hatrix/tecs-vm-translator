import os, sys
import re

class Parser:
    def __init__(self, filename, isdir):
        if not isdir:
            file = open(filename, "r")
            self.content = file.readlines()
           
        else:
            file = open(filename[0], "r")
            self.content = file.readlines()           
 
            # for i in range(len(filename)-1):
                # for val in open(filename[i+1], "r").readlines():
                    # self.content.append(val)
                
        self.commands = list()
        self.line = 0
        
        self.files = list()       
        for val in filename:
            self.files.append(val)
            
        self.current = 0

        self.clean()

    def clean(self):
        for val in self.content:
            if not re.match("//", val):
                if re.search("(.*)(//.*)*", val):
                    if val.strip():
                        expr = "([\(\)A-Z_ \.$;a-z@=0-9\-\+!&|]+)(.*)"
                        command = re.search(expr, val).group(1)
                        if command:
                            self.commands.append(command.strip())
                            
    def nextFile(self):
        if self.current < len(self.files) - 1:
            self.current += 1
            
            self.content = open(self.files[self.current]).readlines()
            self.clean()
            
            return 1
        else:
            return 0
        
    def hasMoreCommands(self):
        if self.line < len(self.commands):
            return True
        else:
            return False
            
    def advance(self):
        self.line += 1
        
    def commandType(self):
        command = self.commands[self.line]
        arithmetics = ["add", "sub", "neg", "eq", "gt", "lt", "and", "or", 
                       "not", "mul", "div", "sqrt"]
                       
        operation = None
        
        # C_ARITHMETIC, C_PUSH, C_POP, C_LABEL, C_GOTO, C_IF, C_FUNCTION,
        # C_RETURN, C_CALL
        
        operation = str()
        
        if command in arithmetics:
            operation = "C_ARITHMETIC"
        
        elif command.split(" ")[0] == "push":
            operation = "C_PUSH"
            
        elif command.split(" ")[0] == "pop":
            operation = "C_POP"
            
        elif command.split(" ")[0] == "label":
            operation = "C_LABEL"
            
        elif command.split(" ")[0] == "goto":
            operation = "C_GOTO"
        
        elif command.split(" ")[0] == "call":
            operation = "C_CALL"
            
        elif command.split(" ")[0] == "return":
            operation = "C_RETURN"
            
        elif command.split(" ")[0] == "function":
            operation = "C_FUNCTION"
            
        elif command.split(" ")[0] == "if-goto":
            operation = "C_IF"
        
        else:
            print("Unknown Operation, maybe just for now.")
            
        return operation
        
    def arg1(self, type):
        command = self.commands[self.line]
        arithmetics = ["add", "sub", "neg", "eq", "gt", "lt", "and", "or", 
                       "not", "mul", "div", "sqrt"]
        
        if command in arithmetics:
            return command
        
        else:
            return command.split(" ")[1]

    def arg2(self, type):
        command = self.commands[self.line]
        
        return command.split(" ")[2]

        
class CodeWriter:
    def __init__(self, filename):
        self.filename = filename.replace(".vm", ".asm")
        self.file = open(self.filename, "w+")
        self.eq = 0
        self.lt = 0
        self.gt = 0
        self.neg = 0
        self.function = str()
        self.calls = list()

    def setFileName(self, filename):
        pass # Used to parse .vm files of directory

    def writeArithmetic(self, command):
        # translate VM into assembly and WRITE IT DOWN.
        # add, sub, neg, eq, gt, lt, and, or, not
        text = list()

        if command == "add":
            # text = ["@SP", "@M", "D=M", "@SP", "M=M-1", "@SP", "@M", "M=M+D"]
            text = ["// Addition", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=D+M"]

        elif command == "mul":
            # text = ["@SP", "@M", "D=M", "@SP", "M=M-1", "@SP", "@M", "M=M+D"]
            text = ["// Mult", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=D*M"]

        elif command == "div":
            # text = ["@SP", "@M", "D=M", "@SP", "M=M-1", "@SP", "@M", "M=M+D"]
            text = ["// Div", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=D/M"]
        
        elif command == "sqrt":
            # text = ["@SP", "@M", "D=M", "@SP", "M=M-1", "@SP", "@M", "M=M+D"]
            text = ["// Sqrt", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=DVM"]

        elif command == "eq":
            eq = self.eq
            text = ["// Equality", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1",
                    "D=D-M", "@EQ%d" %eq, "D;JEQ", "@SP", "A=M-1", "M=0", 
                    "@ENDEQ%d" %eq, "0;JMP", "(EQ%d)" % eq, "@SP", "A=M-1", 
                    "M=-1", "(ENDEQ%d)" % eq]
            self.eq += 1

        elif command == "lt":
            lt = self.lt
            text = ["// Less Than", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1",
                    "D=D-M", "@LT%d" % lt, "D;JGT", "@SP", "A=M-1", "M=0", 
                    "@ENDLT%d" % lt, "0;JMP", "(LT%d)" % lt, "@SP", "A=M-1", 
                    "M=-1", "(ENDLT%d)" % lt]
            self.lt += 1
            
        elif command == "gt":
            gt = self.gt
            text = ["// Greater Than", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1",
                    "D=D-M", "@GT%d" % gt, "D;JLT", "@SP", "A=M-1", "M=0", 
                    "@ENDGT%d" % gt, "0;JMP", "(GT%d)" % gt, "@SP", "A=M-1", 
                    "M=-1", "(ENDGT%d)" % gt]
            self.gt += 1
            
        elif command == "sub":
            text = ["// Substraction", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=M-D"]
            
        elif command == "neg":
            neg = self.neg
            text = ["// Negation", "@SP", "A=M-1", "M=-M"]
                    
        elif command == "and":
            text = ["// And" ,"@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=D&M"]
                    
        elif command == "or":
            text = ["// Or", "@SP", "A=M-1", "D=M", "@SP", "M=M-1", "@SP", "A=M-1", 
                    "M=D|M"]
                    
        elif command == "not":
            text = ["// Not", "@SP", "A=M-1", "M=!M"]
                    
        else:
            pass
            
        self.write(text)
        
    def writePushPop(self, command, segment, index):
        # C_PUSH or C_POP command
        text = list()
        if command == "C_PUSH":
            # add segment index to stack
            # ex : push constant 5 => stack[-1] = 5
            # stack pointer + 1
            if segment == "constant":               
                text = ["// Push constant", "@%s" % index, "D=A", "@SP", "A=M", 
                        "M=D", "@SP", "M=M+1"]
                        
            elif segment == "local":                        
                text = ["// Push local", "@R1", "D=M", "@%s" % index, "A=D+A", 
                        "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1"]
            
            elif segment == "argument":
                text = ["// Push argument", "@R2", "D=M", "@%s" % index, 
                        "A=D+A", "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1"]
                
            elif segment == "this":
                text = ["// Push this", "@R3", "D=M", "@%s" % index, "A=D+A", 
                        "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1"]
               
            elif segment == "that":
                text = ["// Push that", "@R4", "D=M", "@%s" % index, "A=D+A", 
                        "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1"]
            
            elif segment == "temp":
                text = ["// Push temp", "@R5", "D=A", "@%s" % index, "A=D+A",
                        "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1"]
            
            elif segment == "pointer":
                if index == "0": # this
                    text = ["// Push pointer 0", "@R3", "D=M", "@SP", "A=M", 
                            "M=D", "@SP", "M=M+1"]
                        
                elif index == "1": # that
                    text = ["// Push pointer 1", "@R4", "D=M", "@SP", "A=M", 
                            "M=D", "@SP", "M=M+1"]
                        
                else: # should not happen
                    pass
                    
            elif segment == "static":
                name = os.path.basename(self.filename).replace(".asm", "")
                var = name + "." + index
                
                name = os.path.basename(
                                parser.files[parser.current]).replace("vm", "")
                                
                var = name + index
                
                text = ["// Push static", "@%s" % var, "D=M", "@SP", "A=M",
                        "M=D", "@SP", "M=M+1"]
            
            else:
                pass

        elif command == "C_POP":
            if segment == "local":
                text = ["// Pop local",
                        "@R1", "D=M", "@%s" % index, "D=D+A", "@R13", "M=D",
                        "@SP", "A=M-1", "D=M", "@R13", "A=M", "M=D", "@SP",
                        "M=M-1"]
                        
            elif segment == "argument":
                text = ["// Pop argument",
                        "@R2", "D=M", "@%s" % index, "D=D+A", "@R13", "M=D",
                        "@SP", "A=M-1", "D=M", "@R13", "A=M", "M=D", "@SP",
                        "M=M-1"]

            elif segment == "this":
                text = ["// Pop this", 
                        "@R3", "D=M", "@%s" % index, "D=D+A", "@R13", "M=D",
                        "@SP", "A=M-1", "D=M", "@R13", "A=M", "M=D", "@SP",
                        "M=M-1"]
                        
            elif segment == "that":
                text = ["// Pop that",
                        "@R4", "D=M", "@%s" % index, "D=D+A", "@R13", "M=D",
                        "@SP", "A=M-1", "D=M", "@R13", "A=M", "M=D", "@SP",
                        "M=M-1"]
                        
            elif segment == "temp":
                text = ["// Pop temp",
                        "@R5", "D=A", "@%s" % index, "D=D+A", "@R13", "M=D",
                        "@SP", "A=M-1", "D=M", "@R13", "A=M", "M=D", "@SP",
                        "M=M-1"]
                   
            elif segment == "pointer":
                if index == "0": # this
                    text = ["// Pop pointer 0",
                            "@SP", "A=M-1", "D=M", "@R3", "M=D", "@SP", "M=M-1"]
                
                elif index == "1": # that
                    text = ["// Pop pointer 1",
                            "@SP", "A=M-1", "D=M", "@R4", "M=D", "@SP", "M=M-1"]
                
                else: # should not happen either
                    pass
            
            elif segment == "static":
                name = os.path.basename(self.filename).replace(".asm", "")
                var = name + "." + index
                
                name = os.path.basename(
                                parser.files[parser.current]).replace("vm", "")
                                
                var = name + index
                
                print(var)

                
                text = ["// Pop static",
                        "@SP", "A=M-1", "D=M", "@%s" % var, "M=D", "@SP", 
                        "M=M-1"]
                
        else:
            pass # should not happen
        
        self.write(text)

    def writeInit(self):
        text = ["// Bootstrap", "@256", "D=A", "@SP", "M=D", "@Sys.init", 
                "0;JMP"]
        
        self.write(text)
        
    def writeLabel(self, label):
        func = self.function
        text = ["// Label \"%s\" of func \"%s\"" % (label, func),
                "(%s$%s)\n" % (func, label)]
        
        self.write(text)
        
    def writeGoto(self, label):
        func = self.function
        text = ["// Goto %s" % func, "@%s$%s" % (func, label), "0;JMP"]
        
        self.write(text)
        
    def writeIf(self, label):
        func = self.function
        text = ["// If-Goto %s" % func, "@SP", "A=M-1", "D=M", "@SP", "M=M-1", 
                "@%s$%s" % (func, label), "D;JNE"]
        
        self.write(text)
        
    def writeCall(self, functionName, numArgs):
        self.calls.append(functionName)
        text = ["// Call the fuction \"%s\"" % functionName,
                "@return-%s-%d" % (functionName, len(self.calls)), "D=A", "@SP",
                "A=M", "M=D",
                "@SP", "M=M+1", # push return-address
                "@LCL", "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1",  # push LCL
                "@ARG", "D=M", "@SP","A=M", "M=D", "@SP", "M=M+1",   # ARG
                "@THIS", "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1",  # THIS
                "@THAT", "D=M", "@SP", "A=M", "M=D", "@SP", "M=M+1", # THAT
                "@SP", "D=M", "@%s" % numArgs, "D=D-A", "@5", "D=D-A", "@ARG",
                "M=D",                         # reposition ARG
                "@SP", "D=M", "@LCL", "M=D",   # reposition LCL
                "@%s" % functionName, "0;JMP",
                "(return-%s-%d)" % (functionName, len(self.calls))]

        # text = ["@return-%s" % functionName, "D=A", "@SP", "A=M", "M=D", # ret
        
                # "@LCL", "D=M", "@SP", "A=M+1", "M=D",   # push LCL
                
                # "@SP", "D=M", "@2", "D=D+A", "@R13", "M=D", "@ARG", "D=M",
                # "@R13", "A=M", "M=D",   # push ARG
                
                # "@SP", "D=M", "@3", "D=D+A", "@R13", "M=D", "@THIS", "D=M",
                # "@R13", "A=M", "M=D",   # push THIS
                
                # "@SP", "D=M", "@4", "D=D+A", "@R13", "M=D", "@THAT", "D=M",
                # "@R13", "A=M", "M=D",   # push THAT
                
                # "@SP", "D=M", "@%s" % numArgs, "D=D-A", "@ARG", "M=D", # pos ARG
                
                # "@SP", "D=M", "@5", "D=D+A", "@LCL", "M=D", # LCL = SP
                
                # "@SP", "M=D", # Change SP, push
                
                # "@%s" % functionName, "0;JMP"]
                
        self.write(text)
        
    def writeReturn(self):
        text = ["// Function \"%s\" return command" % self.function,
                "@LCL", "D=M", "@R13", "M=D",                   # local on R13
                # "@R13", "D=M", "@5", "D=D-A", "@R14", "M=D",   # ret on R14
                "@R13", "D=M", "@5", "A=D-A", "D=M", "@R14", "M=D", # ret 14 ? 
                "@SP", "A=M-1", "D=M", "@ARG", "A=M", "M=D", #ret
                "@ARG", "D=M+1", "@SP", "M=D",                 # SP
                "@R13", "A=M-1", "D=M", "@THAT", "M=D",        #that
                "@R13", "D=M", "@2", "A=D-A", "D=M", "@THIS", "M=D",#this
                "@R13", "D=M", "@3", "A=D-A", "D=M", "@ARG", "M=D", #arg
                "@R13", "D=M", "@4", "A=D-A", "D=M", "@LCL", "M=D", #lcl
                "@R14", "A=M", "0;JMP"]                        # goto
        
        # text = ["@LCL", "D=M", "@R13", "M=D",
        
                # "@5", "A=D-A", "D=M", "@R14", "M=D",
                
                # "@SP", "A=M-1", "D=M", "@ARG", "A=M", "M=D",
                
                # "@ARG", "D=M+1", "@SP", "M=D", 
                
                # "@R13", "A=M-1", "D=M", "@THAT", "M=D",
                
                # "@R13", "D=M", "@2", "A=D-A", "D=M", "@THIS", "M=D",
                
                # "@R13", "D=M", "@3", "A=D-A", "D=M", "@ARG", "M=D",
                
                # "@R13", "D=M", "@4", "A=D-A", "D=M", "@LCL", "M=D",
                
                # "@R14", "A=M", "0;JMP"]
        
        self.write(text)
        
    def writeFunction(self, functionName, numLocals):
        text = ["// Function \"%s\"" % functionName, "(%s)" % functionName]
        
        self.function = functionName
        
        for i in range(int(numLocals)):
            text.append("@SP")
            text.append("A=M")
            text.append("M=0")
            text.append("@SP")
            text.append("M=M+1")
                
        self.write(text)
    
    def write(self, text):
        for val in text:
            if val == text[-1]:
                self.file.write(val + "\n\n\n")
            else:
             self.file.write(val + "\n")
    
if __name__ == "__main__":

    if os.path.isdir(sys.argv[1]):
        files = list()
        for file in os.listdir(sys.argv[1]):
            if file.endswith(".vm"):
                files.append(os.path.join(sys.argv[1], file))

        parser = Parser(files, 1)
        name = os.path.basename(os.path.normpath(sys.argv[1]))
        writer = CodeWriter(os.path.join(sys.argv[1], name + ".vm"))
        
    else:
        parser = Parser(sys.argv[1], 0)
        writer = CodeWriter(sys.argv[1])
           
    
    if len(sys.argv) > 2:
        if not sys.argv[2] == "-n":
            writer.writeInit()
    else:
        writer.writeInit()

    loop = 1
    while loop:
        if parser.hasMoreCommands():
            type = parser.commandType()
            command = parser.commands[parser.line]
            
            arg_list = ["C_PUSH", "C_POP", "C_FUNCTION", "C_CALL"]
            
            if type != "C_RETURN":
                arg1 = parser.arg1(type)
            
            if type in arg_list:
                arg2 = parser.arg2(type)
            
            # WRITE TIME MOTHERFUCKER
            if type == "C_ARITHMETIC":
                writer.writeArithmetic(command)
            
            elif type == "C_POP" or type == "C_PUSH":
                writer.writePushPop(type, arg1, arg2)
                
            elif type == "C_LABEL":
                writer.writeLabel(arg1)
                
            elif type == "C_IF":
                writer.writeIf(arg1)
                
            elif type == "C_GOTO":
                writer.writeGoto(arg1)
                
            elif type == "C_FUNCTION":
                writer.writeFunction(arg1, arg2)
                
            elif type == "C_RETURN":
                writer.writeReturn()
                
            elif type == "C_CALL":
                writer.writeCall(arg1, arg2)
                
            else:
                pass
        
            parser.advance()
        
        else:
            if parser.nextFile():
                pass
                
            else:
                loop = 0
