// Bootstrap
@256
D=A
@SP
M=D
@Sys.init
0;JMP


// Function "Main.fibonacci"
(Main.fibonacci)


// Push argument
@R2
D=M
@0
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Push constant
@2
D=A
@SP
A=M
M=D
@SP
M=M+1


// Less Than
@SP
A=M-1
D=M
@SP
M=M-1
@SP
A=M-1
D=D-M
@LT0
D;JGT
@SP
A=M-1
M=0
@ENDLT0
0;JMP
(LT0)
@SP
A=M-1
M=-1
(ENDLT0)


// If-Goto Main.fibonacci
@SP
A=M-1
D=M
@SP
M=M-1
@Main.fibonacci$IF_TRUE
D;JNE


// Goto Main.fibonacci
@Main.fibonacci$IF_FALSE
0;JMP


// Label "IF_TRUE" of func "Main.fibonacci"
(Main.fibonacci$IF_TRUE)



// Push argument
@R2
D=M
@0
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Function "Main.fibonacci" return command
@LCL
D=M
@R13
M=D
@R13
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
A=M-1
D=M
@THAT
M=D
@R13
D=M
@2
A=D-A
D=M
@THIS
M=D
@R13
D=M
@3
A=D-A
D=M
@ARG
M=D
@R13
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP


// Label "IF_FALSE" of func "Main.fibonacci"
(Main.fibonacci$IF_FALSE)



// Push argument
@R2
D=M
@0
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Push constant
@2
D=A
@SP
A=M
M=D
@SP
M=M+1


// Substraction
@SP
A=M-1
D=M
@SP
M=M-1
@SP
A=M-1
M=M-D


// Call the fuction "Main.fibonacci"
@return-Main.fibonacci-1
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@1
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(return-Main.fibonacci-1)


// Push argument
@R2
D=M
@0
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Push constant
@1
D=A
@SP
A=M
M=D
@SP
M=M+1


// Substraction
@SP
A=M-1
D=M
@SP
M=M-1
@SP
A=M-1
M=M-D


// Call the fuction "Main.fibonacci"
@return-Main.fibonacci-2
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@1
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(return-Main.fibonacci-2)


// Addition
@SP
A=M-1
D=M
@SP
M=M-1
@SP
A=M-1
M=D+M


// Function "Main.fibonacci" return command
@LCL
D=M
@R13
M=D
@R13
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
A=M-1
D=M
@THAT
M=D
@R13
D=M
@2
A=D-A
D=M
@THIS
M=D
@R13
D=M
@3
A=D-A
D=M
@ARG
M=D
@R13
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP


// Function "Sys.init"
(Sys.init)


// Push constant
@4
D=A
@SP
A=M
M=D
@SP
M=M+1


// Call the fuction "Main.fibonacci"
@return-Main.fibonacci-3
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@1
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(return-Main.fibonacci-3)


// Label "WHILE" of func "Sys.init"
(Sys.init$WHILE)



// Goto Sys.init
@Sys.init$WHILE
0;JMP


