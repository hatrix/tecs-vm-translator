// Bootstrap
@256
D=A
@SP
M=D
@Sys.init
0;JMP


// Function "Class1.set"
(Class1.set)


// Push argument
@R2
D=M
@0
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Pop static
@SP
A=M-1
D=M
@Class1.0
M=D
@SP
M=M-1


// Push argument
@R2
D=M
@1
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Pop static
@SP
A=M-1
D=M
@Class1.1
M=D
@SP
M=M-1


// Push constant
@0
D=A
@SP
A=M
M=D
@SP
M=M+1


// Function "Class1.set" return command
@LCL
D=M
@R13
M=D
@R13
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
A=M-1
D=M
@THAT
M=D
@R13
D=M
@2
A=D-A
D=M
@THIS
M=D
@R13
D=M
@3
A=D-A
D=M
@ARG
M=D
@R13
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP


// Function "Class1.get"
(Class1.get)


// Push static
@Class1.0
D=M
@SP
A=M
M=D
@SP
M=M+1


// Push static
@Class1.1
D=M
@SP
A=M
M=D
@SP
M=M+1


// Substraction
@SP
A=M-1
D=M
@SP
M=M-1
@SP
A=M-1
M=M-D


// Function "Class1.get" return command
@LCL
D=M
@R13
M=D
@R13
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
A=M-1
D=M
@THAT
M=D
@R13
D=M
@2
A=D-A
D=M
@THIS
M=D
@R13
D=M
@3
A=D-A
D=M
@ARG
M=D
@R13
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP


// Function "Class2.set"
(Class2.set)


// Push argument
@R2
D=M
@0
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Pop static
@SP
A=M-1
D=M
@Class2.0
M=D
@SP
M=M-1


// Push argument
@R2
D=M
@1
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1


// Pop static
@SP
A=M-1
D=M
@Class2.1
M=D
@SP
M=M-1


// Push constant
@0
D=A
@SP
A=M
M=D
@SP
M=M+1


// Function "Class2.set" return command
@LCL
D=M
@R13
M=D
@R13
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
A=M-1
D=M
@THAT
M=D
@R13
D=M
@2
A=D-A
D=M
@THIS
M=D
@R13
D=M
@3
A=D-A
D=M
@ARG
M=D
@R13
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP


// Function "Class2.get"
(Class2.get)


// Push static
@Class2.0
D=M
@SP
A=M
M=D
@SP
M=M+1


// Push static
@Class2.1
D=M
@SP
A=M
M=D
@SP
M=M+1


// Substraction
@SP
A=M-1
D=M
@SP
M=M-1
@SP
A=M-1
M=M-D


// Function "Class2.get" return command
@LCL
D=M
@R13
M=D
@R13
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
A=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
A=M-1
D=M
@THAT
M=D
@R13
D=M
@2
A=D-A
D=M
@THIS
M=D
@R13
D=M
@3
A=D-A
D=M
@ARG
M=D
@R13
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP


// Function "Sys.init"
(Sys.init)


// Push constant
@6
D=A
@SP
A=M
M=D
@SP
M=M+1


// Push constant
@8
D=A
@SP
A=M
M=D
@SP
M=M+1


// Call the fuction "Class1.set"
@return-Class1.set
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@2
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class1.set
0;JMP
(return-Class1.set)


// Pop temp
@R5
D=A
@0
D=D+A
@R13
M=D
@SP
A=M-1
D=M
@R13
A=M
M=D
@SP
M=M-1


// Push constant
@23
D=A
@SP
A=M
M=D
@SP
M=M+1


// Push constant
@15
D=A
@SP
A=M
M=D
@SP
M=M+1


// Call the fuction "Class2.set"
@return-Class2.set
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@2
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class2.set
0;JMP
(return-Class2.set)


// Pop temp
@R5
D=A
@0
D=D+A
@R13
M=D
@SP
A=M-1
D=M
@R13
A=M
M=D
@SP
M=M-1


// Call the fuction "Class1.get"
@return-Class1.get
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@0
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class1.get
0;JMP
(return-Class1.get)


// Call the fuction "Class2.get"
@return-Class2.get
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@0
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class2.get
0;JMP
(return-Class2.get)


// Label "WHILE" of func "Sys.init"
(Sys.init$WHILE)



// Goto Sys.init
@Sys.init$WHILE
0;JMP


